% Exemplo 5.2. Matrizes do sistema em malha aberta.

clear all;
A0 = [0 1;0 -3];
A1 = [1 2.4;2.4 -2];
Bw = [1 0]';
Bu = Bw;
Cz0 = [1 1];
Cz1 = [0 0];
Dzu = 0.1;
Cy0 = [1 1];
Cy1 = [1 -1];
Dyw = -0.1;