% Exemplo 5.3. Realiza a realimenta��o de sa�da garantindo estabilidade
% robusta para Hz1w1 e mininimizando a norma H2 de Hz2w2, com a restri��o
% P01 = P02.

function [obj,cont,var] = Design_Example_5_3_LSPI(A0,A1,Bw1,Bw2,Bu,Cz01,Cz02,Cz11,Cz12,Dzu1,Dzu2,Cy0,Cy1,Dyw1,Dyw2)

    n = size(A0,1); % Dimens�o do estados
    r = size(Bw2,2); % Dimens�o do ru�do
    m = size(Bu,2); % Dimens�o do controle
    p = size(Cy0,1); % Dimens�o da sa�da y
    
    setlmis([]); % Inicializando LMIs

    X0 = lmivar(1,[n 1]); % X0 vari�vel matricial n x n simetrica
    X11 = lmivar(1,[n 1]); % X11 vari�vel matricial n x n simetrica
    X21 = lmivar(2,[n n]); % X21 vari�vel matricial n x n
    X22 = lmivar(1,[n 1]); % X22 vari�vel matricial n x n simetrica
    X112 = lmivar(1,[n 1]); % X112 vari�vel matricial n x n simetrica
    X212 = lmivar(2,[n n]); % X212 vari�vel matricial n x n
    X222 = lmivar(1,[n 1]); % X222 vari�vel matricial n x n simetrica
    Y0 = lmivar(1,[n 1]); % Y0 vari�vel matricial n x n simetrica
    L0 = lmivar(2,[m n]); % L0 vari�vel matricial m x n
    L1 = lmivar(2,[m n]); % L1 vari�vel matricial m x n
    Q0 = lmivar(2,[n n]); % Q0 vari�vel matricial n x n
    Q1 = lmivar(2,[n n]); % Q1 vari�vel matricial n x n
    F = lmivar(2,[n p]); % F vari�vel matricial n x p
    W = lmivar(1,[r 1]); % W vari�vel matricial r x r simetrica

%=====================================Hinf w1z1 (gamma = 1)===========================================
    
    % [A0X0+BuL0+(A0X0+BuL0)'+X11   *                            *              *              *               *;
    %  Q0+A0'+X21                   Y0A0+FCy0+(Y0A0+FCy0)'+X22   *              *              *               *;
    %  (A1X0+BuL1)'                 Q1'                          -X11           *              *               *;
    %  A1'                          (Y0A1+FCy1)'                 -X21           -X22           *               *;
    %  Cz01X0+Dzu1L0                Cz01                         Cz11X0+Dzu1L1  Cz11           -I              *;
    %  Bw1'                         (Y0Bw1+FDyw1)'               0              0              0               -I] < 0
    lmiterm([1 1 1 X0],A0,1,'s');
    lmiterm([1 1 1 L0],Bu,1,'s');
    lmiterm([1 1 1 X11],1,1);
    lmiterm([1 2 1 Q0],1,1);
    lmiterm([1 2 1 0],A0');
    lmiterm([1 2 1 X21],1,1);
    lmiterm([1 2 2 Y0],1,A0,'s');
    lmiterm([1 2 2 F],1,Cy0,'s');
    lmiterm([1 2 2 X22],1,1);
    lmiterm([1 3 1 X0],1,A1');
    lmiterm([1 3 1 -L1],1,Bu');
    lmiterm([1 3 2 -Q1],1,1);
    lmiterm([1 3 3 X11],-1,1);
    lmiterm([1 4 1 0],A1');
    lmiterm([1 4 2 Y0],A1',1);
    lmiterm([1 4 2 -F],Cy1',1);
    lmiterm([1 4 3 X21],-1,1);
    lmiterm([1 4 4 X22],-1,1);
    lmiterm([1 5 1 X0],Cz01,1);
    lmiterm([1 5 1 L0],Dzu1,1);
    lmiterm([1 5 2 0],Cz01);
    lmiterm([1 5 3 X0],Cz11,1);
    lmiterm([1 5 3 L1],Dzu1,1);
    lmiterm([1 5 4 0],Cz11);
    lmiterm([1 5 5 0],-1);
    lmiterm([1 6 1 0],Bw1');
    lmiterm([1 6 2 Y0],Bw1',1);
    lmiterm([1 6 2 -F],Dyw1',1);
    lmiterm([1 6 3 0],0);
    lmiterm([1 6 4 0],0);
    lmiterm([1 6 5 0],0);
    lmiterm([1 6 6 0],-1);
    
%================================== H2 w2z2 =============================
    
    % [W             *    *;
    %  Bw2           X0   *;
    %  Y0Bw2+FDyw2   I    Y0] > 0
    lmiterm([-2 1 1 W],1,1);
    lmiterm([-2 2 1 0],Bw2);
    lmiterm([-2 2 2 X0],1,1);
    lmiterm([-2 3 1 Y0],1,Bw2);
    lmiterm([-2 3 1 F],1,Dyw2);
    lmiterm([-2 3 2 0],1);
    lmiterm([-2 3 3 Y0],1,1);

    % [A0X0+BuL0+(A0X0+BuL0)'+X112               *                *                *               *;
    %  Q0+A0'+X212                  Y0A0+FCy0+(Y0A0+FCy0)'+X222   *                *               *;
    %  (A1X0+BuL1)'                 Q1'                           -X112            *               *;
    %  A1'                          (Y0A1+FCy1)'                  -X212            -X222           *;
    %  Cz02X0+Dzu2L0                Cz02                          Cz12X0+Dzu2L1    Cz12            -I] < 0
    lmiterm([3 1 1 X0],A0,1,'s');
    lmiterm([3 1 1 L0],Bu,1,'s');
    lmiterm([3 1 1 X112],1,1);
    lmiterm([3 2 1 Q0],1,1);
    lmiterm([3 2 1 0],A0');
    lmiterm([3 2 1 X212],1,1);
    lmiterm([3 2 2 Y0],1,A0,'s');
    lmiterm([3 2 2 F],1,Cy0,'s');
    lmiterm([3 2 2 X222],1,1);
    lmiterm([3 3 1 X0],1,A1');
    lmiterm([3 3 1 -L1],1,Bu');
    lmiterm([3 3 2 -Q1],1,1);
    lmiterm([3 3 3 X112],-1,1);
    lmiterm([3 4 1 0],A1');
    lmiterm([3 4 2 Y0],A1',1);
    lmiterm([3 4 2 -F],Cy1',1);
    lmiterm([3 4 3 X212],-1,1);
    lmiterm([3 4 4 X222],-1,1);
    lmiterm([3 5 1 X0],Cz02,1);
    lmiterm([3 5 1 L0],Dzu2,1);
    lmiterm([3 5 2 0],Cz02);
    lmiterm([3 5 3 X0],Cz12,1);
    lmiterm([3 5 3 L1],Dzu2,1);
    lmiterm([3 5 4 0],Cz12);
    lmiterm([3 5 5 0],-1);

    lmisys = getlmis; %Finaliza LMIs
    
%========================= Minimiza H2 w2z2 ===============================

    options = [1e-7,2000,0,200,0]; %Op��es do Solver

    c = zeros(decnbr(lmisys),1); % c � vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vW = defcx(lmisys,i,W);
        c(i) = trace(vW); % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = sqrt(copt);
        var.X0 = dec2mat(lmisys,xopt,X0); % Recupera o valor da vari�vel
        var.X11 = dec2mat(lmisys,xopt,X11);
        var.X21 = dec2mat(lmisys,xopt,X21);
        var.X22 = dec2mat(lmisys,xopt,X22);
        var.X112 = dec2mat(lmisys,xopt,X112);
        var.X212 = dec2mat(lmisys,xopt,X212);
        var.X222 = dec2mat(lmisys,xopt,X222);
        var.Y0 = dec2mat(lmisys,xopt,Y0);
        var.L0 = dec2mat(lmisys,xopt,L0);
        var.L1 = dec2mat(lmisys,xopt,L1);
        var.Q0 = dec2mat(lmisys,xopt,Q0);
        var.Q1 = dec2mat(lmisys,xopt,Q1);
        var.F = dec2mat(lmisys,xopt,F);
        var.W = dec2mat(lmisys,xopt,W);
        V0 = eye(n);
        U0 = eye(n)-(var.Y0*var.X0);
        aux1 = [eye(n)/V0 -(V0\var.Y0*Bu);zeros([m,n]) eye(m)];
        aux2 = [var.Q0-(var.Y0*A0*var.X0) var.Q1-(var.Y0*A1*var.X0) var.F;var.L0 var.L1 0];
        aux3 = [eye(n)/U0 zeros(n) zeros([n,m]);zeros(n) eye(n)/U0 zeros([n,m]);-Cy0*var.X0/U0 -Cy1*var.X0/U0 eye([p,m])];
        cont = aux1*aux2*aux3;
    else % Problema infact�vel
        obj = inf;
        var.X0 = []; % Recupera o valor da vari�vel
        var.X11 = [];
        var.X21 = [];
        var.X22 = [];
        var.X112 = [];
        var.X212 = [];
        var.X222 = [];
        var.Y0 = [];
        var.L0 = [];
        var.L1 = [];
        var.Q0 = [];
        var.Q1 = [];
        var.F = [];
        var.W = [];
        cont = [];
    end
end