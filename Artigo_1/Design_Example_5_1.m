% Exemplo 5.1. Realiza a realimenta��o de estados minimizando a norma H2 do
% sistema resultante em malha fechada.

function [obj,K0,K1,var] = Design_Example_5_1(A0,A1,Bw,Bu,Cz0,Cz1,Dzu)

    n = size(A0,1); % Dimens�o do estados
    r = size(Bw,2); % Dimens�o do ru�do
    m = size(Bu,2); % Dimens�o do controle
    
    setlmis([]); % Inicializando LMIs

    X0 = lmivar(1,[n 1]); % X0 vari�vel matricial n x n simetrica
    X1 = lmivar(1,[n 1]); % X1 vari�vel matricial n x n simetrica
    L0 = lmivar(2,[m n]); % L0 vari�vel matricial m x n
    L1 = lmivar(2,[m n]); % L1 vari�vel matricial m x n
    W = lmivar(1,[r 1]); % W vari�vel matricial r x r sim�trica

    % [W   *; 
    %  Bw  X0] > 0
    lmiterm([-1 1 1 W],1,1);
    lmiterm([-1 2 1 0],Bw);
    lmiterm([-1 2 2 X0],1,1);

    % [A0X0+BuL0+(A0X0+BuL0)'+X1    *              *;
    % (A1X0+BuL1)'                  -X1            *;
    % Cz0X0+DzuL0                   Cz1X0+DzuL1    -I] < 0
    lmiterm([2 1 1 X0],A0,1,'s');
    lmiterm([2 1 1 L0],Bu,1,'s');
    lmiterm([2 1 1 X1],1,1);
    lmiterm([2 2 1 X0],1,A1');
    lmiterm([2 2 1 -L1],1,Bu');
    lmiterm([2 2 2 X1],-1,1);
    lmiterm([2 3 1 X0],Cz0,1);
    lmiterm([2 3 1 L0],Dzu,1);
    lmiterm([2 3 2 X0],Cz1,1);
    lmiterm([2 3 2 L1],Dzu,1);
    lmiterm([2 3 3 0],-1);

    lmisys = getlmis; % Finaliza LMIs

    options = [1e-7,2000,0,200,0]; % Op��es do Solver

    c = zeros(decnbr(lmisys),1); % c � o vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vW = defcx(lmisys,i,W);
        c(i) = trace(vW); % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = sqrt(copt);
        var.X0 = dec2mat(lmisys,xopt,X0); % Recupera o valor da vari�vel
        var.X1 = dec2mat(lmisys,xopt,X1);
        var.L0 = dec2mat(lmisys,xopt,L0);
        var.L1 = dec2mat(lmisys,xopt,L1);
        var.W = dec2mat(lmisys,xopt,W);
        K0 = var.L0/var.X0; % K = Z*inv(X)
        K1 = var.L1/var.X0;
    else % Problema infact�vel
        obj = inf;
        var.X0 = []; % Recupera o valor da vari�vel
        var.X1 = [];
        var.L0 = [];
        var.L1 = [];
        var.W = [];
        K0 = [];
        K1 = [];
    end
end