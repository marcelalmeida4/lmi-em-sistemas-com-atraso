% Exemplo 5.1. Matrizes do sistema em malha aberta.

clear all;
A0 = [0 1;0 -1];
A1 = [0 0;2 1];
Bw = eye(2);
Bu = [0 1]';
Cz0 = [1 0;0 1;0 0];
Cz1 = [0 0;0 0;2 1];
Dzu = [0 0 1]';
