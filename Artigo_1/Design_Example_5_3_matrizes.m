% Exemplo 5.3. Matrizes do sistema em malha aberta.

clear all;
A0 = [1 0;0 -2];
A1 = [0.5 0.32;2 -0.5];
Bw1 = [0.47 0.75]';
Bw2 = [1 0 0;0 1 0];
Bu = Bw1;
Cz01 = [0.71 0.89];
Cz02 = [1 0;0 1;0 0;0 0;0 0];
Cz11 = [0 0];
Cz12 = [0 0;0 0;1 0;0 1;0 0];
Dzw1 = 0;
Dzw2 = zeros([5,3]);
Dzu1 = 0;
Dzu2 = [0 0 0 0 0.1]';
Cy0 = [-1 0];
Cy1 = [0 0];
Dyw1 = 0;
Dyw2 = [0 0 0.1];