% Exemplo 5.2. Realiza a realimenta��o de sa�da minimizando a norma H2 do
% sistema em malha fechada.

function [obj,cont,var] = Design_Example_5_2(A0,A1,Bw,Bu,Cz0,Cz1,Dzu,Cy0,Cy1,Dyw)

    n = size(A0,1); % Dimens�o do estados
    r = size(Bw,2); % Dimens�o do ru�do
    m = size(Bu,2); % Dimens�o do controle
    p = size(Cy0,1); % Dimens�o da sa�da y
    
    setlmis([]); % Inicializando LMIs

    X0 = lmivar(1,[n 1]); % X0 vari�vel matricial n x n simetrica
    X11 = lmivar(1,[n 1]); % X11 vari�vel matricial n x n simetrica
    X21 = lmivar(2,[n n]); % X21 vari�vel matricial n x n
    X22 = lmivar(1,[n 1]); % X22 vari�vel matricial n x n simetrica
    Y0 = lmivar(1,[n 1]); % Y0 vari�vel matricial n x n simetrica
    L0 = lmivar(2,[m n]); % L0 vari�vel matricial m x n
    L1 = lmivar(2,[m n]); % L1 vari�vel matricial m x n
    Q0 = lmivar(2,[n n]); % Q0 vari�vel matricial n x n
    Q1 = lmivar(2,[n n]); % Q1 vari�vel matricial n x n
    F = lmivar(2,[n p]); % F vari�vel matricial n x p
    W = lmivar(1,[r 1]); % W vari�vel matricial r x r

    % [W           *    *;
    %  Bw          X0   *;
    %  Y0Bw+FDyw   I    Y0] > 0
    lmiterm([-1 1 1 W],1,1);
    lmiterm([-1 2 1 0],Bw);
    lmiterm([-1 2 2 X0],1,1);
    lmiterm([-1 3 1 Y0],1,Bw);
    lmiterm([-1 3 1 F],1,Dyw);
    lmiterm([-1 3 2 0],1);
    lmiterm([-1 3 3 Y0],1,1);

    % [A0X0+BuL0+(A0X0+BuL0)'+X11   *                            *               *          *;
    %  Q0+A0'+X21                   Y0A0+FCy0+(Y0A0+FCy0)'+X22   *               *          *;
    %  (A1X0+BuL1)'                 Q1'                          -X11            *          *;
    %  A1'                          (Y0A1+FCy1)'                 -X21            -X22       *;
    %  Cz0X0+DzuL0                  Cz0                          Cz1X0+DzuL1     Cz1        -I] < 0
    lmiterm([2 1 1 X0],A0,1,'s');
    lmiterm([2 1 1 L0],Bu,1,'s');
    lmiterm([2 1 1 X11],1,1);
    lmiterm([2 2 1 Q0],1,1);
    lmiterm([2 2 1 0],A0');
    lmiterm([2 2 1 X21],1,1);
    lmiterm([2 2 2 Y0],1,A0,'s');
    lmiterm([2 2 2 F],1,Cy0,'s');
    lmiterm([2 2 2 X22],1,1);
    lmiterm([2 3 1 X0],1,A1');
    lmiterm([2 3 1 -L1],1,Bu');
    lmiterm([2 3 2 -Q1],1,1);
    lmiterm([2 3 3 X11],-1,1);
    lmiterm([2 4 1 0],A1');
    lmiterm([2 4 2 Y0],A1',1);
    lmiterm([2 4 2 -F],Cy1',1);
    lmiterm([2 4 3 X21],-1,1);
    lmiterm([2 4 4 X22],-1,1);
    lmiterm([2 5 1 X0],Cz0,1);
    lmiterm([2 5 1 L0],Dzu,1);
    lmiterm([2 5 2 0],Cz0);
    lmiterm([2 5 3 X0],Cz1,1);
    lmiterm([2 5 3 L1],Dzu,1);
    lmiterm([2 5 4 0],Cz1);
    lmiterm([2 5 5 0],-1);

    lmisys = getlmis; %Finaliza LMIs

    options = [1e-7,2000,0,200,0]; %Op��es do Solver

    c = zeros(decnbr(lmisys),1); % c � vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vW = defcx(lmisys,i,W);
        c(i) = trace(vW); % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = sqrt(copt);
        var.X0 = dec2mat(lmisys,xopt,X0); % Recupera o valor da vari�vel
        var.X11 = dec2mat(lmisys,xopt,X11);
        var.X21 = dec2mat(lmisys,xopt,X21);
        var.X22 = dec2mat(lmisys,xopt,X22);
        var.Y0 = dec2mat(lmisys,xopt,Y0);
        var.L0 = dec2mat(lmisys,xopt,L0);
        var.L1 = dec2mat(lmisys,xopt,L1);
        var.Q0 = dec2mat(lmisys,xopt,Q0);
        var.Q1 = dec2mat(lmisys,xopt,Q1);
        var.F = dec2mat(lmisys,xopt,F);
        var.W = dec2mat(lmisys,xopt,W);
        V0 = eye(n);
        U0 = eye(n)-(var.Y0*var.X0);
        aux1 = [eye(n)/V0 -(V0\var.Y0*Bu);zeros([m,n]) eye(m)];
        aux2 = [var.Q0-(var.Y0*A0*var.X0) var.Q1-(var.Y0*A1*var.X0) var.F;var.L0 var.L1 0];
        aux3 = [eye(n)/U0 zeros(n) zeros([n,m]);zeros(n) eye(n)/U0 zeros([n,m]);-Cy0*var.X0/U0 -Cy1*var.X0/U0 eye([p,m])];
        cont = aux1*aux2*aux3;
    else % Problema infact�vel
        obj = inf;
        var.X0 = []; % Recupera o valor da vari�vel
        var.X11 = [];
        var.X21 = [];
        var.X22 = [];
        var.Y0 = [];
        var.L0 = [];
        var.L1 = [];
        var.Q0 = [];
        var.Q1 = [];
        var.F = [];
        var.W = [];
        cont = [];
    end
end