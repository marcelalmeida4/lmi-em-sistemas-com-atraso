	Esses arquivos cont�m as implementa��es dos exemplos pr�ticos contidos no artigo:

[3] Wu, F., \& Grigoriadis, K. M. (2001). LPV systems with parameter-varying time delays: analysis and control. \textit{Automatica, 37(2)}, 221-229..

	Este artigo cont�m dois exemplos, sendo que no primeiro � projetado um controlador para um sistema LPV com atraso no estado, enquanto no segundo o sistema em quest�o � um sistema LPV com atraso na entrada de controle.
	Dessa maneira, na implementa��o de cada exemplo foram utilizados os seguintes arquivos: 

* Sistema LPV com atraso no estado

	geraMatrizes.m
	resolveGrid.m
	resolveGridMiVar.m
	figura_3.m
	sim1.slx
	sim1_mycont.slx

* Sistema LPV com atraso na entrada de controle

	geraMatrizes2.m
	resolveGrid2.m

	Al�m disso, os arquivos 'geraMatrizes.m' e 'geraMatrizes2.m' recebem como argumento um dado valor do par�metro variante no tempo 'rho1' e retornam as repectivas matrizes dos sistemas em malha aberta, conforme descrito abaixo:
	
[A,Ah,B1,B2,C1,D12,n] = geraMatrizes(rho1)

	rho1 = Par�metro variante no tempo
	A = Din�mica do sistema
	Ah = Din�mica do sistema (Atraso)
	B1 = Entrada externa
	B2 = Entrada de controle
	C1 = Estado na sa�da
	D12 = Entrada de controle na sa�da
	n = Dimens�o do estado

[A,Ah,B1,B2,C1,D12,n] = geraMatrizes2(rho1)

	rho1 = Par�metro variante no tempo
	A = Din�mica do sistema
	Ah = Din�mica do sistema (Atraso)
	B1 = Entrada externa
	B2 = Entrada de controle
	C1 = Estado na sa�da
	D12 = Entrada de controle na sa�da
	n = Dimens�o do estado

	Ademais, os argumentos e vari�veis de retorno dos demais c�digos s�o aqueles apresentados abaixo:

* Sistema LPV com atraso no estado

[obj,var] = resolveGrid()

	obj = Norma H_inf �tima
	var = Vari�veis das LMIs

[obj,var] = resolveGridMiVar(mi)

	mi = Limite superior para o par�metro do atraso
	obj = Norma H_inf �tima
	var = Vari�veis das LMIs

* Sistema LPV com atraso na entrada de controle

[obj,var] = resolveGrid2()

	obj = Norma H_inf �tima
	var = Vari�veis das LMIs

	Finalmente, os arquivos com extens�o '.slx' cont�m as simula��es do sistema em malha fechada, obtido com o uso do controlador projetado no exemplo do sistema com atraso no estado, sendo que o arquivo 'sim1.slx' utiliza o controlador apresentado no texto original, enquanto no arquivo 'sim1_mycont.slx' o controlador utilizado � aquele obtido pelo autor desta revis�o.