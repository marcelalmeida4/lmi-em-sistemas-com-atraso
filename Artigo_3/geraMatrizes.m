% Gera as matrizes do sistema LPV com atraso no estado, para um valor
% espec�fico do par�metro rho1. 

function [A,Ah,B1,B2,C1,D12,n] = geraMatrizes(rho1)

    fi = 0.2;
    delta = 0.1;

    A = [0 (1+fi*rho1);-2 (-3+delta*rho1)];
    Ah = [(fi*rho1) 0.1;(-0.2+delta*rho1) -0.3];
    B1 = [0.2 0.2]';
    B2 = [(fi*rho1) (0.1+delta*rho1)]';
    C1 = [0 1;0 0];
    D12 = [0 1]';
    n = size(A,1); % Dimens�o do estado

end