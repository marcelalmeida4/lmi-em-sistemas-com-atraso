% Exemplo num�rico de controle �timo H_inf, por realimenta��o de estado,
% de um sistema LPV com atraso no estado para um dado valor do limite do
% atraso (mi).

function [obj,var] = resolveGridMiVar(mi)

    rho1 = linspace(-1,1,9); % rho1 => [-1,1]
    rho2 = linspace(0,1,9); % rho2 => [0,1]
    v1 = 1; % limite para drho1/dt
    v2 = 5; % limite para drho2/dt
    nlmi = 1; % Vari�vel para contar as LMIs criadas
    
    setlmis([]); % Inicializando LMIs
    
    [~,~,~,~,~,~,n] = geraMatrizes(rho1(1));
    R1 = lmivar(1,[n 1]); % R1 vari�vel matricial n x n simetrica
    R2 = lmivar(1,[n 1]); % R2 vari�vel matricial n x n simetrica
    R3 = lmivar(1,[n 1]); % R3 vari�vel matricial n x n simetrica
    S = lmivar(1,[n 1]); % S vari�vel matricial n x n simetrica
    gamma = lmivar(1,[1 0]); % gamma vari�vel escalar

    % Implementa o grid 9x9 no espa�o rho1xrho2
    for i=1:1:9 % Itera rho1
        [A,Ah,B1,B2,C1,~,~] = geraMatrizes(rho1(i));
        C11 = C1(1,:);
        C12 = C1(2,:);
        Ac = A-(B2*C12);
        Ahc = Ah;
        for j=1:1:9 % Itera rho2
            
            % R(rho) = R1+rho1(i)*R2+rho2(j)*R3 > 0
            lmiterm([-nlmi 1 1 R1],1,1);
            lmiterm([-nlmi 1 1 R2],rho1(i),1);
            lmiterm([-nlmi 1 1 R3],rho2(j),1);
            
            % [T11                  (*)   (*)        (*)
            %  R(rho)               -S    (*)        (*)
            %  B1'(rho)             0     -gamma*I   (*)
            %  C11(rho)R(rho)+T41   0     0          -gamma*I+T44] < 0
            % =====================================================================================
            % T11 = R(rho)Ac'(rho)+Ac(rho)R(rho)-sum+psi*Ahc(rho)*S*Ahc'(rho)-gamma*B2(rho)B2'(rho)
            % R(rho) = R1+rho1(i)R2+rho2(j)R3
            % sum = v1(dR/drho1)+v2(dR/drho2)
            % psi = (1-(v1(dh/drho1)+v2(dh/drho2)))^-1
            % h(rho) = mi*rho2(j)
            % T41 = psi*C11h(rho)*S*Ahc'(rho)
            % T44 = psi*C11h(rho)*S*C11h'(rho)
            lmiterm([(nlmi+1) 1 1 R1],Ac,1,'s');
            lmiterm([(nlmi+1) 1 1 R2],rho1(i)*Ac,1,'s');
            lmiterm([(nlmi+1) 1 1 R3],rho2(j)*Ac,1,'s');
            lmiterm([(nlmi+1) 1 1 R2],-v1,1);
            lmiterm([(nlmi+1) 1 1 R3],-v2,1);
            lmiterm([(nlmi+1) 1 1 S],((1-(v2*mi))^-1)*Ahc,Ahc');
            lmiterm([(nlmi+1) 1 1 gamma],-1,B2*B2');
            lmiterm([(nlmi+1) 2 1 R1],1,1);
            lmiterm([(nlmi+1) 2 1 R2],rho1(i),1);
            lmiterm([(nlmi+1) 2 1 R3],rho2(j),1);
            lmiterm([(nlmi+1) 2 2 S],-1,1);
            lmiterm([(nlmi+1) 3 1 0],B1');
            lmiterm([(nlmi+1) 3 3 gamma],-1,1);
            lmiterm([(nlmi+1) 4 1 R1],C11,1);
            lmiterm([(nlmi+1) 4 1 R2],rho1(i)*C11,1);
            lmiterm([(nlmi+1) 4 1 R3],rho2(j)*C11,1);
            lmiterm([(nlmi+1) 4 4 gamma],-1,1);
            
            % [T11                  (*)   (*)        (*)
            %  R(rho)               -S    (*)        (*)
            %  B1'(rho)             0     -gamma*I   (*)
            %  C11(rho)R(rho)+T41   0     0          -gamma*I+T44] < 0
            % =====================================================================================
            % T11 = R(rho)Ac'(rho)+Ac(rho)R(rho)-sum+psi*Ahc(rho)*S*Ahc'(rho)-gamma*B2(rho)B2'(rho)
            % R(rho) = R1+rho1(i)R2+rho2(j)R3
            % sum = v1(dR/drho1)-v2(dR/drho2)
            % psi = (1-(v1(dh/drho1)-v2(dh/drho2)))^-1
            % h(rho) = mi*rho2(j)
            % T41 = psi*C11h(rho)*S*Ahc'(rho)
            % T44 = psi*C11h(rho)*S*C11h'(rho)
            lmiterm([(nlmi+2) 1 1 R1],Ac,1,'s');
            lmiterm([(nlmi+2) 1 1 R2],rho1(i)*Ac,1,'s');
            lmiterm([(nlmi+2) 1 1 R3],rho2(j)*Ac,1,'s');
            lmiterm([(nlmi+2) 1 1 R2],-v1,1);
            lmiterm([(nlmi+2) 1 1 R3],v2,1);
            lmiterm([(nlmi+2) 1 1 S],((1+(v2*mi))^-1)*Ahc,Ahc');
            lmiterm([(nlmi+2) 1 1 gamma],-1,B2*B2');
            lmiterm([(nlmi+2) 2 1 R1],1,1);
            lmiterm([(nlmi+2) 2 1 R2],rho1(i),1);
            lmiterm([(nlmi+2) 2 1 R3],rho2(j),1);
            lmiterm([(nlmi+2) 2 2 S],-1,1);
            lmiterm([(nlmi+2) 3 1 0],B1');
            lmiterm([(nlmi+2) 3 3 gamma],-1,1);
            lmiterm([(nlmi+2) 4 1 R1],C11,1);
            lmiterm([(nlmi+2) 4 1 R2],rho1(i)*C11,1);
            lmiterm([(nlmi+2) 4 1 R3],rho2(j)*C11,1);
            lmiterm([(nlmi+2) 4 4 gamma],-1,1);
            
            % [T11                  (*)   (*)        (*)
            %  R(rho)               -S    (*)        (*)
            %  B1'(rho)             0     -gamma*I   (*)
            %  C11(rho)R(rho)+T41   0     0          -gamma*I+T44] < 0
            % =====================================================================================
            % T11 = R(rho)Ac'(rho)+Ac(rho)R(rho)-sum+psi*Ahc(rho)*S*Ahc'(rho)-gamma*B2(rho)B2'(rho)
            % R(rho) = R1+rho1(i)R2+rho2(j)R3
            % sum = -v1(dR/drho1)+v2(dR/drho2)
            % psi = (1-(-v1(dh/drho1)+v2(dh/drho2)))^-1
            % h(rho) = mi*rho2(j)
            % T41 = psi*C11h(rho)*S*Ahc'(rho)
            % T44 = psi*C11h(rho)*S*C11h'(rho)
            lmiterm([(nlmi+3) 1 1 R1],Ac,1,'s');
            lmiterm([(nlmi+3) 1 1 R2],rho1(i)*Ac,1,'s');
            lmiterm([(nlmi+3) 1 1 R3],rho2(j)*Ac,1,'s');
            lmiterm([(nlmi+3) 1 1 R2],v1,1);
            lmiterm([(nlmi+3) 1 1 R3],-v2,1);
            lmiterm([(nlmi+3) 1 1 S],((1-(v2*mi))^-1)*Ahc,Ahc');
            lmiterm([(nlmi+3) 1 1 gamma],-1,B2*B2');
            lmiterm([(nlmi+3) 2 1 R1],1,1);
            lmiterm([(nlmi+3) 2 1 R2],rho1(i),1);
            lmiterm([(nlmi+3) 2 1 R3],rho2(j),1);
            lmiterm([(nlmi+3) 2 2 S],-1,1);
            lmiterm([(nlmi+3) 3 1 0],B1');
            lmiterm([(nlmi+3) 3 3 gamma],-1,1);
            lmiterm([(nlmi+3) 4 1 R1],C11,1);
            lmiterm([(nlmi+3) 4 1 R2],rho1(i)*C11,1);
            lmiterm([(nlmi+3) 4 1 R3],rho2(j)*C11,1);
            lmiterm([(nlmi+3) 4 4 gamma],-1,1);
            
            % [T11                  (*)   (*)        (*)
            %  R(rho)               -S    (*)        (*)
            %  B1'(rho)             0     -gamma*I   (*)
            %  C11(rho)R(rho)+T41   0     0          -gamma*I+T44] < 0
            % =====================================================================================
            % T11 = R(rho)Ac'(rho)+Ac(rho)R(rho)-sum+psi*Ahc(rho)*S*Ahc'(rho)-gamma*B2(rho)B2'(rho)
            % R(rho) = R1+rho1(i)R2+rho2(j)R3
            % sum = -v1(dR/drho1)-v2(dR/drho2)
            % psi = (1-(-v1(dh/drho1)-v2(dh/drho2)))^-1
            % h(rho) = mi*rho2(j)
            % T41 = psi*C11h(rho)*S*Ahc'(rho)
            % T44 = psi*C11h(rho)*S*C11h'(rho)
            lmiterm([(nlmi+4) 1 1 R1],Ac,1,'s');
            lmiterm([(nlmi+4) 1 1 R2],rho1(i)*Ac,1,'s');
            lmiterm([(nlmi+4) 1 1 R3],rho2(j)*Ac,1,'s');
            lmiterm([(nlmi+4) 1 1 R2],v1,1);
            lmiterm([(nlmi+4) 1 1 R3],v2,1);
            lmiterm([(nlmi+4) 1 1 S],((1+(v2*mi))^-1)*Ahc,Ahc');
            lmiterm([(nlmi+4) 1 1 gamma],-1,B2*B2');
            lmiterm([(nlmi+4) 2 1 R1],1,1);
            lmiterm([(nlmi+4) 2 1 R2],rho1(i),1);
            lmiterm([(nlmi+4) 2 1 R3],rho2(j),1);
            lmiterm([(nlmi+4) 2 2 S],-1,1);
            lmiterm([(nlmi+4) 3 1 0],B1');
            lmiterm([(nlmi+4) 3 3 gamma],-1,1);
            lmiterm([(nlmi+4) 4 1 R1],C11,1);
            lmiterm([(nlmi+4) 4 1 R2],rho1(i)*C11,1);
            lmiterm([(nlmi+4) 4 1 R3],rho2(j)*C11,1);
            lmiterm([(nlmi+4) 4 4 gamma],-1,1);
            
            nlmi = nlmi+5;
        
        end
    end
    
    lmisys = getlmis; %Finaliza LMIs
    
    options = [1e-7,2000,0,200,0]; %Op��es do Solver

    c = zeros(decnbr(lmisys),1); % c � vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vgamma = defcx(lmisys,i,gamma);
        c(i) = vgamma; % Define o vetor de custo
    end
    
    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = copt;
        var.R1 = dec2mat(lmisys,xopt,R1); % Recupera o valor da vari�vel
        var.R2 = dec2mat(lmisys,xopt,R2);
        var.R3 = dec2mat(lmisys,xopt,R3);
        var.S = dec2mat(lmisys,xopt,S);
        var.gamma = dec2mat(lmisys,xopt,gamma);
    else % Problema infact�vel
        obj = inf;
        var.R1 = []; % Recupera o valor da vari�vel
        var.R2 = [];
        var.R3 = [];
        var.S = [];
        var.gamma = [];
    end
end