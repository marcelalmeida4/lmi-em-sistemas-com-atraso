% Gera a figura 3 apresentada no texto.

for i=0:1:9
    mi(i+1) = 0.01*i;
    [obj(i+1),~] = resolveGridMiVar(mi(i+1));
end

close all;
figure
plot(mi,obj,'b--','linewidth',2)
title('Magnitude m�xima do atraso vs performance Hinf')
xlabel('Magnitude do atraso m�ximo (mi)')
ylabel('Performance - Hinf')