    function [obj,K,var] = Realim_Estado_H2_Discreto(A,B,E,C,D,F)

    n = size(A,1); % Dimensao do estados
    q = size(E,2); % Dimensao de saida
    m = size(B,2); % Dimensao do controle
    
    setlmis([]); % Inicializando LMIs

    X = lmivar(1,[n 1]); % X variavel matricial n x n simetrica
    W = lmivar(1,[q 1]); % W variavel matricial q x q simetrica
    Z = lmivar(2,[m n]); % Z variavel matricial m x n

    % [ W * *; E X *; F 0 I ] > 0
    lmiterm([-1 1 1 W],1,1);
    lmiterm([-1 2 1 0],E);
    lmiterm([-1 2 2 X],1,1);
    lmiterm([-1 3 1 0],F);
    lmiterm([-1 3 2 0],0);
    lmiterm([-1 3 3 0],1);
    
    % [X * *; AX+BZ X *; CX+DZ 0 I] > 0
    lmiterm([-2 1 1 X],1,1);
    lmiterm([-2 2 1 X],A,1);
    lmiterm([-2 2 1 Z],B,1);
    lmiterm([-2 2 2 X],1,1);
    lmiterm([-2 3 1 X],C,1);
    lmiterm([-2 3 1 Z],D,1);
    lmiterm([-2 3 2 0],0);
    lmiterm([-2 3 3 0],1);

    lmisys = getlmis; % Finaliza LMIs

    options = [1e-7,2000,0,200,0]; % Opcoes do Solver

    c = zeros(decnbr(lmisys),1); % c e o vetor de zeros da dimensao do numero de variaveis escalares do problema
    for i = 1:decnbr(lmisys)
        vW = defcx(lmisys,i,W);
        c(i) = trace(vW); % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja factivel
        obj = sqrt(copt);
        var.X = dec2mat(lmisys,xopt,X); % Recupera o valor da variavel
        var.W = dec2mat(lmisys,xopt,W);
        var.Z = dec2mat(lmisys,xopt,Z);
        K = var.Z/var.X;
    else % Problema infactivel
        obj = inf;
        K = [];
        var.X = [];
        var.W = [];
        var.Z = [];
    end


end