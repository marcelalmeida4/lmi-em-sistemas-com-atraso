function [obj,var] = lmi_exemplo(A,C)

    n = size(A,1); % Dimens�o do estados

    setlmis([]); % Inicializando LMIs

    P = lmivar(1,[n 1]); % P vari�vel matricial n x n simetrica

    % P > 0
	lmiterm([-1 1 1 P],1,1);
        
    % A'P + PA + C'C < 0
    lmiterm([2 1 1 P],A',1,'s');
    lmiterm([2 1 1 0],C'*C);
    
    lmisys = getlmis; %Finaliza LMIs
    options = [1e-7,2000,0,200,0]; %Op��es do Solver

    c = zeros(decnbr(lmisys),1); % c � vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vP = defcx(lmisys,i,P);
        c(i) = trace(vP); % Define o vetor de custo
    end
    
    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = copt;
        var.P = dec2mat(lmisys,xopt,P); % Recupera o valor da vari�vel
    else % Problema infact�vel
        obj = inf;
        var.P = [];
    end
    
end