function [obj,var] = norm_h2_disc(A,E,C,F)

    n = size(A,1); % Dimensao do estados
    m = size(E,2); % Dimensao da entrada
    p = size(C,1); % Dimensao de saida

    setlmis([]); % Inicializando LMIs

    P = lmivar(1,[n 1]); % P variavel matricial n x n simetrica

    % P > 0
	lmiterm([-1 1 1 P],1,1);
        
    % A'PA - P + C'C < 0
    lmiterm([2 1 1 P],A',A);
    lmiterm([2 1 1 P],-1,1);
    lmiterm([2 1 1 0],C'*C);
  % APA' - P + EE' < 0
  % lmiterm([2 1 1 P],A,A');
  % lmiterm([2 1 1 P],-1,1);
  % lmiterm([2 1 1 0],E*E');
        
    lmisys = getlmis; % Finaliza LMIs
    
    options = [1e-7,2000,0,200,0]; % Opcoes do Solver

    c = zeros(decnbr(lmisys),1); % c e o vetor de zeros da dimensao do numero de variaveis escalares do problema
    for i = 1:decnbr(lmisys)
        vP = defcx(lmisys,i,P);
        c(i) = trace(E'*vP*E); % Define o vetor de custo para P => Po
        % c(i) = trace(C*vP*C'); % Define o vetor de custo para P => Pc
    end
    
    
    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja factivel
        obj = sqrt(copt+trace(F'*F));
      % obj = sqrt(copt+trace(F*F'));
        var.P = dec2mat(lmisys,xopt,P); % Recupera o valor da variavel
    else % Problema infactivel
        obj = inf;
        var.P = [];
    end
end