function [obj,var] = norm_hinf_cont(A,E,C,F)

    n = size(A,1); % Dimensao do estados
    m = size(E,2); % Dimensao da entrada
    p = size(C,1); % Dimensao de saida

    setlmis([]); % Inicializando LMIs

    P = lmivar(1,[n 1]); % P variavel matricial n x n simetrica
    gamma = lmivar(1,[1 0]);

    % P > 0
	lmiterm([-1 1 1 P],1,1);
        
    % [ A'P + PA + C'C,          *    ;
    %   E'P + F'C,      F'F - gamma*I  ] < 0
    lmiterm([2 1 1 P],A',1,'s');
    lmiterm([2 1 1 0],C'*C);
    lmiterm([2 2 1 P],E',1);
    lmiterm([2 2 1 0],F'*C);
    lmiterm([2 2 2 0],F'*F);
    lmiterm([2 2 2 gamma],-1,1);
        
    lmisys = getlmis; % Finaliza LMIs
    
    options = [1e-7,2000,0,200,0]; % Opcoes do Solver

    c = zeros(decnbr(lmisys),1); % c e o vetor de zeros da dimensao do numero de variaveis escalares do problema
    for i = 1:decnbr(lmisys)
        vgamma = defcx(lmisys,i,gamma);
        c(i) = vgamma; % Define o vetor de custo
    end
    
    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja factivel
        obj = sqrt(copt);
        var.P = dec2mat(lmisys,xopt,P); % Recupera o valor da variavel
        var.gamma = dec2mat(lmisys,xopt,gamma);
    else % Problema infactivel
        obj = inf;
        var.P = [];
        var.gamma = [];
    end
    
end