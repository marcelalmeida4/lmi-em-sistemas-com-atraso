function [obj,var] = lmi_custo(A,C,X0) % Minimiza X0'*P*X0

    n = size(A,1); % Dimensao dos estados

    setlmis([]); % Inicializando LMIs

    P = lmivar(1,[n 1]); % P variavel matricial n x n simetrica

    % P > 0
	lmiterm([-1 1 1 P],1,1);
        
    % A'P + PA + C'C < 0
    lmiterm([2 1 1 P],A',1,'s');
    lmiterm([2 1 1 0],C'*C);
    
    lmisys = getlmis; % Finaliza LMIs
    options = [1e-7,2000,0,200,0]; % Opcoes do Solver
  
    c = zeros(decnbr(lmisys),1); % c e vetor de zeros da dimensao do numero de variaveis escalares do problema
    for i = 1:decnbr(lmisys)
        vP = defcx(lmisys,i,P);
        c(i) = X0'*vP*X0; % Define o vetor de custo
    end
    
    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja factivel
        obj = copt;
        var.P = dec2mat(lmisys,xopt,P); % Recupera o valor da variavel
    else % Problema infactivel
        obj = inf;
        var.P = [];
    end
    
end