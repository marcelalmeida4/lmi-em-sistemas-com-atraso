function [obj,var] = Filtro_H2_Continuo(A,E,Cz,Ez,Cy,Ey)

    n = size(A,1); % Dimensao do estados
    q = size(E,2); % Dimensao da entrada externa
    p = size(Cz,1);% Dimensao da saida
    
    setlmis([]); % Inicializando LMIs

    Z = lmivar(1,[n 1]); % Z variavel matricial n x n simetrica
    F = lmivar(2,[n p]); % F variavel matricial n x p
    Q = lmivar(1,[n 1]); % Q variavel matricial n x n simetrica
    L = lmivar(2,[p n]); % L variavel matricial p x n 
    W = lmivar(1,[q 1]); % W variavel matricial q x q simetrica
    Y = lmivar(1,[n 1]); % Y variavel matricial n x n simetrica
    
    % [ W * *; ZE Z *; YE+FEy Z Y] > 0
    lmiterm([-1 1 1 W],1,1);
    lmiterm([-1 2 1 Z],1,E);
    lmiterm([-1 2 2 Z],1,1);
    lmiterm([-1 3 1 Y],1,E);
    lmiterm([-1 3 1 F],1,Ey);
    lmiterm([-1 3 2 Z],1,1);
    lmiterm([-1 3 3 Y],1,1);

    % [ZA+A'Z * *; YA+FCy+Q+A'Z YA+A'Y+FCy+Cy'F' *; Cz-RCy-L Cz-RCy -I] < 0
    % Com R=0 => Df=0 => Ez=0
    lmiterm([2 1 1 Z],1,A,'s');
    lmiterm([2 2 1 Y],1,A);
    lmiterm([2 2 1 F],1,Cy);
    lmiterm([2 2 1 Q],1,1);
    lmiterm([2 2 1 Z],A',1);
    lmiterm([2 2 2 Y],1,A,'s');
    lmiterm([2 2 2 F],1,Cy,'s');
    lmiterm([2 3 1 0],Cz);
    lmiterm([2 3 1 L],-1,1);
    lmiterm([2 3 2 0],Cz);
    lmiterm([2 3 3 0],-1);

    lmisys = getlmis; % Finaliza LMIs

    options = [1e-7,2000,0,200,0]; % Opcoes do Solver

    c = zeros(decnbr(lmisys),1); % c e o vetor de zeros da dimensao do numero de variaveis escalares do problema
    for i = 1:decnbr(lmisys)
        vW = defcx(lmisys,i,W);
        c(i) = trace(vW); % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja factivel
        obj = sqrt(copt);
        var.Z = dec2mat(lmisys,xopt,Z); % Recupera o valor da variavel
        var.F = dec2mat(lmisys,xopt,F);
        var.Q = dec2mat(lmisys,xopt,Q);
        var.L = dec2mat(lmisys,xopt,L);
        var.W = dec2mat(lmisys,xopt,W);
        var.Y = dec2mat(lmisys,xopt,Y);
        
        % Calculo das matrizes Af, Bf e Cf do filtro
        % Assumindo Df = R = 0
        I = eye(n);
        V = eye(n);
        U = (I-(var.Z\var.Y))/(V');
        var.Af = ((V\var.Q)/var.Z)/(U');
        var.Bf = V\var.F;
        var.Cf = (var.L/var.Z)/(U');
        
    else % Problema infactivel
        obj = inf;
        var.Z = [];
        var.F = [];
        var.Q = [];
        var.L = [];
        var.W = [];
        var.Y = [];
        var.Af = [];
        var.Bf = [];
        var.Cf = [];
    end
end