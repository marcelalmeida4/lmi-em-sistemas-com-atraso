    function [obj,K,var] = Realim_Estado_Hinf_Continuo(A,B,E,C,D,F)

    n = size(A,1); % Dimensao do estados
    q = size(E,2); % Dimensao de saida
    m = size(B,2); % Dimensao do controle
    
    setlmis([]); % Inicializando LMIs

    gamma = lmivar(1,[1 0]);
    X = lmivar(1,[n 1]); % X variavel matricial n x n simetrica
    Z = lmivar(2,[m n]); % Z variavel matricial m x n

    % X > 0 => P > 0
    lmiterm([-1 1 1 X],1,1);
    
    % [ AX+XA'+BZ+Z'B' * *; E' -gamma*I *; CX+DZ F -I ] < 0
    lmiterm([2 1 1 X],A,1,'S');
    lmiterm([2 2 1 0],E');
    lmiterm([2 2 2 gamma],-1,1);
    lmiterm([2 3 1 X],C,1);
    lmiterm([2 3 1 Z],D,1);
    lmiterm([2 3 2 0],F);
    lmiterm([2 3 3 0],-1);

    lmisys = getlmis; % Finaliza LMIs

    options = [1e-7,2000,0,200,0]; % Opcoes do Solver

    c = zeros(decnbr(lmisys),1); % c e o vetor de zeros da dimensao do numero de variaveis escalares do problema
    for i = 1:decnbr(lmisys)
        vgamma = defcx(lmisys,i,gamma);
        c(i) = vgamma; % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja factivel
        obj = sqrt(copt);
        var.X = dec2mat(lmisys,xopt,X); % Recupera o valor da variavel
        var.Z = dec2mat(lmisys,xopt,Z);
        K = var.Z/var.X;
    else % Problema infactivel
        obj = inf;
        K = [];
        var.X = [];
        var.Z = [];
    end


end