% Exemplo Num�rico com o m�todo proposto. Realiza a realimenta��o de estado minimizando 
% a norma H2 do sistema em malha fechada com gamma_inf = 0.46.

function [obj,K,var] = Numerical_Example_Proposed(A,At,Bw,Bu,Ci,Cti,C2,Ct2,Dui,Du2,di,d2)

    n = size(A,1); % Dimens�o do estados
    s = size(Bw,2); % Dimens�o do ru�do
    l = size(Bu,2); % Dimens�o do controle
    m = size(Ci,1); % Dimens�o da sa�da z
    
    setlmis([]); % Inicializando LMIs

    Qi0 = lmivar(1,[n 1]); % Qi0 vari�vel matricial n x n simetrica
    Qi1 = lmivar(1,[n 1]); % Qi1 vari�vel matricial n x n simetrica
    Q20 = lmivar(1,[n 1]); % Q20 vari�vel matricial n x n simetrica
    Q21 = lmivar(1,[n 1]); % Q21 vari�vel matricial n x n simetrica
    F = lmivar(2,[n n]); % F vari�vel matricial n x n
    M = lmivar(2,[l n]); % M vari�vel matricial l x n
    W = lmivar(1,[s 1]); % W vari�vel matricial s x s simetrica
   
    %=============================== Hinf gamma = 0.46 =============================================;

    % [AF+F'A'+BuM+M'Bu'  Qi0-F'+di(AF+BuM)  F'Ci'+M'Dui'      Bw       AtQi1   Qi0;
    %  *                  -di(F+F')          di(F'Ci'+M'Dui')  0        0       0;
    %  *                  *                  -I                0        CtiQi1  0;
    %  *                  *                  *                 -0.46*I  0       0;
    %  *                  *                  *                 *        -Qi1    0;
    %  *                  *                  *                 *        *       -Qi1] < 0
    lmiterm([1 1 1 F],A,1,'s');
    lmiterm([1 1 1 M],Bu,1,'s');
    lmiterm([1 1 2 Qi0],1,1);
    lmiterm([1 1 2 -F],-1,1);
    lmiterm([1 1 2 F],di*A,1);
    lmiterm([1 1 2 M],di*Bu,1);
    lmiterm([1 1 3 -F],1,Ci');
    lmiterm([1 1 3 -M],1,Dui');
    lmiterm([1 1 4 0],Bw);
    lmiterm([1 1 5 Qi1],At,1);
    lmiterm([1 1 6 Qi0],1,1);
    lmiterm([1 2 2 F],-di,1,'s');
    lmiterm([1 2 3 -F],di,Ci');
    lmiterm([1 2 3 -M],di,Dui');
    lmiterm([1 3 3 0],-1);
    lmiterm([1 3 5 Qi1],Cti,1);
    lmiterm([1 4 4 0],-0.46);
    lmiterm([1 5 5 Qi1],-1,1);
    lmiterm([1 6 6 Qi1],-1,1);
    
    % Qi0 > 0
    lmiterm([-2 1 1 Qi0],1,1);
    
%============================================ H2 ==================================================;
  
    % [W  Bw';
    %  *  Q20] > 0
    lmiterm([-3 1 1 W],1,1);
    lmiterm([-3 1 2 0],Bw');
    lmiterm([-3 2 2 Q20],1,1);
 
    % [AF+F'A'+BuM+M'Bu'  Q20-F'+d2(AF+BuM)  F'C2'+M'Du2'     AtQ21   Q20;
    %  *                  -d2(F+F')          d2(F'C2'+M'Du2') 0       0;
    %  *                  *                  -I               Ct2Q21  0;
    %  *                  *                  *                -Q21    0;
    %  *                  *                  *                *       -Q21] < 0
    lmiterm([4 1 1 F],A,1,'s');
    lmiterm([4 1 1 M],Bu,1,'s');
    lmiterm([4 1 2 Q20],1,1);
    lmiterm([4 1 2 -F],-1,1);
    lmiterm([4 1 2 F],d2*A,1);
    lmiterm([4 1 2 M],d2*Bu,1);
    lmiterm([4 1 3 -F],1,C2');
    lmiterm([4 1 3 -M],1,Du2');
    lmiterm([4 1 4 Q21],At,1);
    lmiterm([4 1 5 Q20],1,1);
    lmiterm([4 2 2 F],-d2,1,'s');
    lmiterm([4 2 3 -F],d2,C2');
    lmiterm([4 2 3 -M],d2,Du2');
    lmiterm([4 3 3 0],-1);
    lmiterm([4 3 4 Q21],Ct2,1);
    lmiterm([4 4 4 Q21],-1,1);
    lmiterm([4 5 5 Q21],-1,1);

    lmisys = getlmis; %Finaliza LMIs

    options = [1e-7,2000,0,200,0]; %Op��es do Solver

    c = zeros(decnbr(lmisys),1); % c � vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vW = defcx(lmisys,i,W);
        c(i) = trace(vW); % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = sqrt(copt);
        var.Qi0 = dec2mat(lmisys,xopt,Qi0); % Recupera o valor da vari�vel
        var.Qi1 = dec2mat(lmisys,xopt,Qi1);
        var.Q20 = dec2mat(lmisys,xopt,Q20);
        var.Q21 = dec2mat(lmisys,xopt,Q21);
        var.F = dec2mat(lmisys,xopt,F);
        var.M = dec2mat(lmisys,xopt,M);
        var.W = dec2mat(lmisys,xopt,W);
        K = var.M/var.F;
    else % Problema infact�vel
        obj = inf;
        var.Qi0 = []; % Recupera o valor da vari�vel
        var.Qi1 = [];
        var.Q20 = [];
        var.Q21 = [];
        var.F = [];
        var.M = [];
        var.W = [];
        K = [];
    end
end