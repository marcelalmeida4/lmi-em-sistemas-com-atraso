% Exemplo num�rico. Matrizes do sistema em malha aberta.

clear all;
A = [0 0 1 0;0 0 0 1;-0.09 0.09 -0.0038 0.0038;0.09 -0.09 0.0038 -0.0038];
At = [0 0 0 0;0 0 0 0;-0.01 0 0 0;0 -0.001 0 -0.001];
Bw = [0 0 0 1]';
Bu = [0 0 1 0]';
Ci = [0 0.95 0 0;0 0 0 0];
Cti = zeros([2 4]);
Dui = [0 0.01]';
C2 = [0 1 0 0;0 0 0 0];
Ct2 = zeros([2 4]);
Du2 = [0 0.01]';
