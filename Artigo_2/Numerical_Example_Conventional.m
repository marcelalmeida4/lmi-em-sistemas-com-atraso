% Exemplo Num�rico com o m�todo convencional. Realiza a realimenta��o de estado minimizando
% a norma H2 do sistema em malha fechada, com gamma_inf = 0.46 e Qi0 = Q20.

function [obj,K,var] = Numerical_Example_Conventional(A,At,Bw,Bu,Ci,Cti,C2,Ct2,Dui,Du2)

    n = size(A,1); % Dimens�o do estados
    s = size(Bw,2); % Dimens�o do ru�do
    l = size(Bu,2); % Dimens�o do controle
    m = size(Ci,1); % Dimens�o da sa�da z
    
    setlmis([]); % Inicializando LMIs

    Qi0 = lmivar(1,[n 1]); % Qi0 vari�vel matricial n x n simetrica
    Qi1 = lmivar(1,[n 1]); % Qi1 vari�vel matricial n x n simetrica
    Q21 = lmivar(1,[n 1]); % Q21 vari�vel matricial n x n simetrica
    M = lmivar(2,[l n]); % M vari�vel matricial l x n
    W = lmivar(1,[s 1]); % W vari�vel matricial s x s simetrica
   
    %=============================== Hinf gamma = 0.46 =============================================;

    % [AQi0+Qi0A'+BuM+M'Bu'  Qi0C'+M'Du'  Bw       AtQi1   Qi0;
    %  *                     -I           0        CtiQi1  0;
    %  *                     *            -0.46*I  0       0;
    %  *                     *            *        -Qi1    0;
    %  *                     *            *        *       -Qi1] < 0
    lmiterm([1 1 1 Qi0],A,1,'s');
    lmiterm([1 1 1 M],Bu,1,'s');
    lmiterm([1 1 2 Qi0],1,Ci');
    lmiterm([1 1 2 -M],1,Dui');
    lmiterm([1 1 3 0],Bw);
    lmiterm([1 1 4 Qi1],At,1);
    lmiterm([1 1 5 Qi0],1,1);
    lmiterm([1 2 2 0],-1);
    lmiterm([1 2 4 Qi1],Cti,1);
    lmiterm([1 3 3 0],-0.46);
    lmiterm([1 4 4 Qi1],-1,1);
    lmiterm([1 5 5 Qi1],-1,1);
    
%============================================ H2 ==================================================;
  
    % [W  Bw';
    %  *  Qi0] > 0
    lmiterm([-2 1 1 W],1,1);
    lmiterm([-2 1 2 0],Bw');
    lmiterm([-2 2 2 Qi0],1,1);
 
    % [AQi0+Qi0A'+BuM+M'Bu'  Qi0C'+M'Du'  AtQ21   Q20;
    %  *                     -I           Ct2Q21  0;
    %  *                     *            -Q21    0;
    %  *                     *            *       -Q21] < 0
    lmiterm([3 1 1 Qi0],A,1,'s');
    lmiterm([3 1 1 M],Bu,1,'s');
    lmiterm([3 1 2 Qi0],1,C2');
    lmiterm([3 1 2 -M],1,Du2');
    lmiterm([3 1 3 Q21],At,1);
    lmiterm([3 1 4 Qi0],1,1);
    lmiterm([3 2 2 0],-1);
    lmiterm([3 2 3 Q21],Ct2,1);
    lmiterm([3 3 3 Q21],-1,1);
    lmiterm([3 4 4 Q21],-1,1);

    lmisys = getlmis; %Finaliza LMIs

    options = [1e-7,2000,0,200,0]; %Op��es do Solver

    c = zeros(decnbr(lmisys),1); % c � vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vW = defcx(lmisys,i,W);
        c(i) = trace(vW); % Define o vetor de custo
    end

    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = sqrt(copt);
        var.Qi0 = dec2mat(lmisys,xopt,Qi0); % Recupera o valor da vari�vel
        var.Qi1 = dec2mat(lmisys,xopt,Qi1);
        var.Q21 = dec2mat(lmisys,xopt,Q21);
        var.M = dec2mat(lmisys,xopt,M);
        var.W = dec2mat(lmisys,xopt,W);
        K = var.M/var.Qi0;
    else % Problema infact�vel
        obj = inf;
        var.Qi0 = []; % Recupera o valor da vari�vel
        var.Qi1 = [];
        var.Q21 = [];
        var.M = [];
        var.W = [];
        K = [];
    end
end