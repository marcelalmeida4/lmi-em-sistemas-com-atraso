% Gera as matrizes utilizadas no exemplo

close all
clear all
clc

A = [0 1;-1 1];
A1 = [0 0;0.1 0.1];
B = [0 1]';
A0 = [1 1;0 0];
A11 = [1 1;0 0];
B1 = [0 1]';
D = [0.1 0.1 0.1;0 0 0.1];
E1 = [1 1;0 0;0 0];
E2 = [0 0 1]';
Ed = [0 0;1 1;0 0];
Q = eye(2);
R = 1;
fiz = [exp(1) 0]';