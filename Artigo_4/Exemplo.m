% Determina o ganho �timo de realimenta��o de estado para o sistema incerto
% com atraso apresentado como exemplo.

function [obj,K,var] = Exemplo(A,A1,B,D,E1,E2,Ed,Q,R,fiz)
    
    n = size(A,1); % Dimens�o dos estados
    m = size(B,2); % Dimens�o do controle
    
    setlmis([]); % Inicializando LMIs
    
    eps = lmivar(1,[1 0]); % eps vari�vel escalar
    alpha = lmivar(1,[1 0]); % alpha vari�vel escalar
    W = lmivar(2,[m n]); % W vari�vel matricial m x n
    X = lmivar(1,[n 1]); % X vari�vel matricial n x n simetrica
    V = lmivar(1,[n 1]); % V vari�vel matricial n x n simetrica
    M = lmivar(1,[m 1]); % M vari�vel matricial n x n simetrica
    N = lmivar(2,[n m]); % N vari�vel matricial n x n

    % [Ah        *     *       *       *       *;
    %  VA1'      -V    *       *       *       *;
    %  E1X+E2W   EdV   -epsI   *       *       *;
    %  X         0     0       -Q^-1   *       *;
    %  W         0     0       0       -R^-1   *;
    %  X         0     0       0       0       -V] < 0
    %
    %  Ah = AX + BW +(AX+BW)' + epsDD'
    lmiterm([1 1 1 X],A,1,'s');
    lmiterm([1 1 1 W],B,1,'s');
    lmiterm([1 1 1 eps],1,D*D');
    lmiterm([1 2 1 V],1,A1');
    lmiterm([1 2 2 V],-1,1);
    lmiterm([1 3 1 X],E1,1);
    lmiterm([1 3 1 W],E2,1);
    lmiterm([1 3 2 V],Ed,1);
    lmiterm([1 3 3 eps],-1,1);
    lmiterm([1 4 1 X],1,1);
    lmiterm([1 4 4 0],-inv(Q));
    lmiterm([1 5 1 W],1,1);
    lmiterm([1 5 5 0],-inv(R));
    lmiterm([1 6 1 X],1,1);
    lmiterm([1 6 6 V],-1,1);
    
    % [-alpha   *;
    %   fiz    -X] < 0
    lmiterm([2 1 1 alpha],-1,1);
    lmiterm([2 2 1 0],fiz);
    lmiterm([2 2 2 X],-1,1);
    
    % [-M   *;
    %   N  -V] < 0
    lmiterm([3 1 1 M],-1,1);
    lmiterm([3 2 1 N],1,1);
    lmiterm([3 2 2 V],-1,1);
    
    % X > 0
    lmiterm([-4 1 1 X],1,1);
        
    lmisys = getlmis; % Finaliza LMIs

    options = [1e-7,2000,0,200,0]; % Op��es do Solver
    
    c1 = zeros(decnbr(lmisys),1); % c1 � vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        valpha = defcx(lmisys,i,alpha);
        c1(i) = valpha; % Define o vetor de custo
    end
    
    c2 = zeros(decnbr(lmisys),1); % c2 � o vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    for i = 1:decnbr(lmisys)
        vM = defcx(lmisys,i,M);
        c2(i) = trace(vM); % Define o vetor de custo
    end
    
    c = zeros(decnbr(lmisys),1); % c � o vetor de zeros da dimens�o do n�mero de vari�veis escalares do problema
    c = c1 + c2;
    
    [copt,xopt] = mincx(lmisys,c,options); % Resolve o problema
    if(~isempty(copt)) % Caso seja fact�vel
        obj = copt;
        var.W = dec2mat(lmisys,xopt,W); % Recupera o valor da vari�vel
        var.X = dec2mat(lmisys,xopt,X);
        var.V = dec2mat(lmisys,xopt,V);
        var.M = dec2mat(lmisys,xopt,M);
        var.N = dec2mat(lmisys,xopt,N);
        var.eps = dec2mat(lmisys,xopt,eps);
        var.alpha = dec2mat(lmisys,xopt,alpha);
        K = var.W/var.X;
    else % Problema infact�vel
        obj = inf;
        var.W = []; % Recupera o valor da vari�vel
        var.X = [];
        var.V = [];
        var.M = [];
        var.N = [];
        var.eps = [];
        var.alpha = [];
        K = [];
    end        
end